﻿Feature: SearchNews

Background: 
Given user navigate to News page

@mytag
Scenario: Check that the title of main article the News is correct
    When get the main article title
    Then the title of main article is "Trump and Biden duel in chaotic, bitter debate"

Scenario: Check that the planned secondary articles are published
    When user get a list of secondary articles
    Then the planned secondary articles are present on the page
    | articles                                            |
    | The surge from one Covid death to one million       |
    | Armenia says its fighter jet 'shot down by Turkey'  |
    | How will Trump try to dominate debate this time?    |
    | TV crew finds new evidence in Estonia ship disaster |
    | Chadwick Boseman shared salary with Sienna Miller   |

Scenario: Check that search world is present in the title
    Given get category of first article
    When search articles by this category
    Then the first article will contain this word in the title