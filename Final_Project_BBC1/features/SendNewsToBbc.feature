﻿Feature: SendNewsToBbc	

Background: 
Given user navigate to News page

@mytag
Scenario Outline: Check that news aren't sent without all correct data
Given user navigate to Share News with BBC page
And Fill form
| 'Tell us your story. ' | 'Name' | 'Email address' | 'Contact number ' | 'Location ' |
| <story>                | <name> | <email>         | <number>          | <location>  |
When user accept obligatory fields
Then the news isn't sent and get <error>
 Examples:
| story              | name  | email                 | number        | location | error                    |
| I want to be a QA. |       | yulbulleodd@gmail.com | +380673679201 | Kyiv     | Name can't be blank      |
|                    |       |                       |               |          |                          |
| I want to be a QA. | Yulia | yulbulleodd@gmail     | +380673679201 | Kyiv     | Email address is invalid |




