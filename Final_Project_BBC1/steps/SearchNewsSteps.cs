﻿using System;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Final_Project_BBC1.Business_Logic_Layer;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow.Assist;
using Final_Project_BBC1.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Final_Project_BBC1.steps
{
    [Binding]
    public class SearchNewsSteps
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly SearchNews searchNews;
        

        public SearchNewsSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            searchNews = _scenarioContext.Get<SearchNews>("searchNewsLayer");            
        }

        [Given(@"user navigate to News page")]
        public void GivenUserNavigateToNewsPage()
        {
            searchNews.OpenNewsPage();
            searchNews.SkipPopUp();
        }

        [Given(@"get category of first article")]
        public void GivenGetCategoryOfFirstArticle()
        {
            searchNews.GetCategoryTheFirstArticle();
        }

        [When(@"get the main article title")]
        public void WhenGetTheMainArticleTitle()
        {
            string mainArticleTitle = searchNews.GetTheMainArticleTitle();
            _scenarioContext.Add("mainArticleTitle", mainArticleTitle);
        }

        [When(@"user get a list of secondary articles")]
        public void WhenUserGetAListOfSecondaryArticles()
        {
            List<string> publishedArticles = searchNews.GetThePublishedArticleTitles();
            _scenarioContext.Add("publishedArticles", publishedArticles);
        }

        [When(@"search articles by this category")]
        public void WhenSearchArticlesByThisCategory()
        {
            string category = searchNews.GetCategoryTheFirstArticle();
            _scenarioContext.Add("category", category);
            searchNews.SearchNewsByCategoryName(category);
        }

        [Then(@"the title of main article is ""(.*)""")]
        public void ThenTheTitleOfMailArticleIs(string mainTitle)
        {
            string mainArticleTitle = _scenarioContext.Get<string>("mainArticleTitle");
            Assert.IsTrue(mainArticleTitle.Contains(mainTitle), "The article title is incorrect");
        }
        
        [Then(@"the planned secondary articles are present on the page")]
        public void ThenThePlannedSecondaryArticlesArePresentOnThePage(Table articles)
        {            
            IEnumerable<string> titles = articles.CreateSet<string>();
            List<string> articleTitles = new List<string>();
            foreach (string article in titles)
            {
                string title = article.Trim();
                articleTitles.Add(title);                
            }
            List<string> publishedArticles = _scenarioContext.Get<List<string>>("publishedArticles");
            Assert.IsTrue(searchNews.GetThePublishedArticleTitles().SequenceEqual(articleTitles), "The lists are different.");
        }        

        [Then(@"the first article will contain this word in the title")]
        public void ThenTheFirstArticleWillContainThisWordInTheTitle()
        {
            string category = _scenarioContext.Get<string>("category");
            Assert.IsTrue(searchNews.GetFirstTitleSearchedByCategory().Contains(category), "The search world isn't found.");
        }        
    }
}
