﻿using System;
using TechTalk.SpecFlow;
using Final_Project_BBC1.Business_Logic_Layer;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow.Assist;
using Final_Project_BBC1.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Final_Project_BBC1.hooks;
using OpenQA.Selenium.Chrome;

namespace Final_Project_BBC1.steps
{
    [Binding]
    public class SendNewsToBbcSteps
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly SendNewsToBbc sendNewsToBbc;

        public SendNewsToBbcSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            sendNewsToBbc = _scenarioContext.Get<SendNewsToBbc>("sendNewsToBbcLayer");
        }


        [Given(@"user navigate to Share News with BBC page")]
        public void GivenUserNavigateToShareNewsWithBBCPage()
        {
            sendNewsToBbc.OpenShareNewsWithBbcPage();
        }
        
        [Given(@"Fill form")]
        public void GivenFillForm(Table table)
        {
            var dictionary = new Dictionary<string, string>();
            ICollection<string> placehold = table.Header;
            for(int i = 0; i < table.Header.Count; i++)
            {
                dictionary.Add(placehold.ElementAt(i), table.Rows[0][i]);
            }
            sendNewsToBbc.FillFormToShareNews(dictionary);            
        }

        [When(@"user accept obligatory fields")]
        public void WhenUserAcceptObligatoryFields()
        {
            sendNewsToBbc.AcceptTheObligatoryFields();
        }

        [Then(@"the news isn't sent and get (.*)")]
        public void ThenTheNewsIsnTSentAndGet(string error)
        {
            string errorMessageWeb = sendNewsToBbc.GetTheErrorMessage();            
            if(error.Equals(""))
            {
                Assert.IsTrue(sendNewsToBbc.GetErrorMessages().Displayed);
            }
            else if (error != null)
            {
                Assert.AreEqual(errorMessageWeb, error, "Error messages aren't equal.");
            }
        }
    }
}
