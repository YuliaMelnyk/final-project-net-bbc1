﻿using System.Collections.Generic;
using Final_Project_BBC1.Business_Logic_Layer;
using Final_Project_BBC1.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Final_Project_BBC1.tests
{
    [TestClass]
    public class SendQuestionTests : BaseTests
    {                      
        private Dictionary<string, string> dict = new Dictionary<string, string>() { 
            { "'Tell us your story. '", "I want to be a QA." },
            { "'Name'", " "},
            { "'Email address'", "yulbulleodd@gmail.com"},
            { "'Contact number '", "+380673679201"},
            { "'Location '", "Kyiv"} };
        private Dictionary<string, string> dictEmpty = new Dictionary<string, string>() { 
            { "'Tell us your story. '", " " },
            { "'Name'", " "},
            { "'Email address'", " "},
            { "'Contact number '", " "},
            { "'Location '", " "} };
        private Dictionary<string, string> dictWithIncorrectEmail = new Dictionary<string, string>() {
            { "'Tell us your story. '", "I want to be a QA." },
            { "'Name'", "Yulia"},
            { "'Email address'", "yulbulleodd@gmail"},
            { "'Contact number '", "+380673679201"},
            { "'Location '", "Kyiv"} };
        private string nameMessageError = "Name can't be blank";
        private string emailErrorMessage = "Email address is invalid";


        [TestMethod]        
        public void CheckTheQuestionIsNotSentWithoutEnteredName()
        {            
            GetSearchNews().OpenNewsPage();
            GetSearchNews().SkipPopUp();
            GetSendNewsToBbc().OpenShareNewsWithBbcPage();
            GetSendNewsToBbc().FillFormToShareNews(dict);
            GetSendNewsToBbc().AcceptTheObligatoryFields();
            string nameMessageErrorWeb = GetSendNewsToBbc().GetTheErrorMessage();
            Assert.AreEqual(nameMessageErrorWeb, nameMessageError, "The error message is different.");
        }

        [TestMethod]
        public void CheckTheQuestionIsNotSentWithEmptyFields()
        {            
            GetSearchNews().OpenNewsPage();
            GetSearchNews().SkipPopUp();
            GetSendNewsToBbc().OpenShareNewsWithBbcPage();
            GetSendNewsToBbc().FillFormToShareNews(dictEmpty);
            GetSendNewsToBbc().AcceptTheObligatoryFields();
            Assert.IsTrue(GetSendNewsToBbc().GetErrorMessages().Displayed, "The error message is absent.");
        }

        [TestMethod]
        public void CheckTheQuestionIsNotSentWithIncorrectEmail()
        {            
            GetSearchNews().OpenNewsPage();
            GetSearchNews().SkipPopUp();
            GetSendNewsToBbc().OpenShareNewsWithBbcPage();
            GetForm().FillForm(dictWithIncorrectEmail);
            GetSendNewsToBbc().AcceptTheObligatoryFields();
            Assert.AreEqual(GetSendNewsToBbc().GetTheErrorMessage(), emailErrorMessage, "The error messages aren't equal.");
        }
    }
}
