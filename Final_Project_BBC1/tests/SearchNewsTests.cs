﻿using Final_Project_BBC1.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Final_Project_BBC1.tests
{
    [TestClass]
    public class SearchNewsTests : BaseTests
    {
        private string mainArticle = "Trump and Biden duel in chaotic, bitter debate";
        private List<string> articleTitlesPlan = new List<string> {
        "The surge from one Covid death to one million",
        "Armenia says its fighter jet 'shot down by Turkey'",
        "How will Trump try to dominate debate this time?",
        "TV crew finds new evidence in Estonia ship disaster",        
        "Chadwick Boseman shared salary with Sienna Miller" };
        
        [TestMethod]
        public void CheckTitleMainArticle()
        {            
            GetSearchNews().OpenNewsPage();
            GetSearchNews().SkipPopUp();
            Assert.IsTrue(GetSearchNews().GetTheMainArticleTitle().Contains(mainArticle), "The article title is incorrect.");
        }

        [TestMethod]
        public void CheckThatPlannedTitlesArePublished()
        {            
            GetSearchNews().OpenNewsPage();
            GetSearchNews().SkipPopUp();            
            Assert.IsTrue(GetSearchNews().GetThePublishedArticleTitles().SequenceEqual(articleTitlesPlan), "The lists are different.");
        }

        [TestMethod]
        public void CheckTheSearchWordIsPresent()
        {            
            GetSearchNews().OpenNewsPage();
            GetSearchNews().SkipPopUp();            
            string category = GetSearchNews().GetCategoryTheFirstArticle();
            GetSearchNews().SearchNewsByCategoryName(category);
            Assert.IsTrue(GetSearchNews().GetFirstTitleSearchedByCategory().Contains(category), "The search world isn't found.");
        }
    }
}
