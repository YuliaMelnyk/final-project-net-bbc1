﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Final_Project_BBC1.pages;
using Final_Project_BBC1.Business_Logic_Layer;

namespace Final_Project_BBC1.tests
{
    [TestClass]
    public class BaseTests
    {
        private IWebDriver driver;
        private SearchNews searchNews;
        private SendNewsToBbc sendNewsToBbc;
        

        [TestInitialize]
        public void TestInitialize()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com/");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);            
        }        

        [TestCleanup]
         public void TestCleanUp()
        {
            if (driver != null)
                driver.Quit();
        }

        
        public IWebDriver GetDriver()
        {
            return driver;
        }                
        
        public Form GetForm()
        {
            return new Form(GetDriver());
        }

        public SearchNews GetSearchNews()
        {
            if (searchNews == null) searchNews = new SearchNews(driver);
            return searchNews;
        }

        public SendNewsToBbc GetSendNewsToBbc()
        {
            if (sendNewsToBbc == null) sendNewsToBbc = new SendNewsToBbc(driver);
            return sendNewsToBbc;
        }        
    }
}
