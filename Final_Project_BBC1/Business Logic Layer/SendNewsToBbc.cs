﻿using System.Collections.Generic;
using Final_Project_BBC1.pages;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Final_Project_BBC1.Business_Logic_Layer
{
    public class SendNewsToBbc
    {
        private IWebDriver driver;

        public SendNewsToBbc(IWebDriver driver)
        {
            this.driver = driver;
        }
        

        public void OpenShareNewsWithBbcPage()
        {            
            NewsPage newsPage = new NewsPage(driver);
            CoronavirusPage coronavirusPage = new CoronavirusPage(driver);            
            newsPage.ClickOnCoronavirusButton();
            coronavirusPage.ClickOnYourCoronavirusStoriesButton();
            coronavirusPage.ClickOnShareWithBbcNews();            
        }

        public void FillFormToShareNews(Dictionary<string, string> dict)
        {
            Form form = new Form(driver);
            form.FillForm(dict);            
        }

        public void AcceptTheObligatoryFields()
        {
            ShareWithBbcNewsPage sharePage = new ShareWithBbcNewsPage(driver);
            sharePage.ClickOnIamOver16YearsOldButton();
            sharePage.ClickOnIacceptButton();
            sharePage.ClickOnSubmitButton();
        }

        public string GetTheErrorMessage()
        {
            ShareWithBbcNewsPage sharePage = new ShareWithBbcNewsPage(driver);
            return sharePage.GetErrorMessageWeb();
        }

        public IWebElement GetErrorMessages()
        {
            ShareWithBbcNewsPage sharePage = new ShareWithBbcNewsPage(driver);
            return sharePage.GetErrorMessages();
        }        
    }
}
