﻿using System.Collections.Generic;
using Final_Project_BBC1.pages;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Final_Project_BBC1.Business_Logic_Layer
{
    public class SearchNews
    {
        private IWebDriver driver;        

        public SearchNews(IWebDriver driver)
        {
            this.driver = driver;            
        }        

        public void OpenNewsPage()
        {
            HomePage homePage = new HomePage(driver);
            homePage.ClickOnNewsButton();
        }

        public void SkipPopUp()
        {
            NewsPage newsPage = new NewsPage(driver);            
            newsPage.SkipPopUp();
        }

        public string GetTheMainArticleTitle()
        {
            NewsPage newsPage = new NewsPage(driver);            
            return newsPage.GetMainArticleWebTitle();
        }

        public List<string> GetThePublishedArticleTitles()
        {
            NewsPage newsPage = new NewsPage(driver);            
            return newsPage.GetSecondaryArticleTitles();
        }

        public string GetCategoryTheFirstArticle()
        {
            NewsPage newsPage = new NewsPage(driver);
            return newsPage.GetCategoryText();
        }

        public void SearchNewsByCategoryName(string category)
        {
            NewsPage newsPage = new NewsPage(driver);            
            category = GetCategoryTheFirstArticle();
            newsPage.EnterTheCategory(category);
            newsPage.SearchByTheCategory();
        }

        public string GetFirstTitleSearchedByCategory()
        {
            SearchPage searchPage = new SearchPage(driver);
            return searchPage.GetSearchArticleTitle();
        }
    }
}
