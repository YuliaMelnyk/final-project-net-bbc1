﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Final_Project_BBC1.pages
{
    public class CoronavirusPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'secondary-sections')]//a[contains(@class,'nw-o-link')]")]
        private IWebElement YourCoronavirusStoriesButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'c-5-slice')]//a[contains(@href,'10725415')]")]
        private IWebElement ShareWithBbcNews { get; set; }
        
        public CoronavirusPage(IWebDriver driver) : base(driver)
        {

        }

        public void ClickOnYourCoronavirusStoriesButton()
        {
            YourCoronavirusStoriesButton.Click();
        }

        public void ClickOnShareWithBbcNews()
        {
            ShareWithBbcNews.Click();
        }
    }
}
