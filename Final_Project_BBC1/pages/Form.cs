﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Final_Project_BBC1.pages
{
    public class Form
    {        
        IWebDriver driver;          

        public Form(IWebDriver driver) 
        {
            this.driver = driver;            
        }

        private IWebElement GetFormElement(string field) => driver.FindElement(By.XPath($"//*[@placeholder={field}]"));

        public void FillForm(Dictionary<string, string> fields)
        {
            foreach (var key in fields.Keys)
            {
                GetFormElement(key).SendKeys(fields[key]);
            }
        }
    }
}
