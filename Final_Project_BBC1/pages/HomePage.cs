﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Final_Project_BBC1.pages
{
    public class HomePage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'nav-pri-container')]//a[contains(text(),'News')]")]
        private IWebElement NewsButton { get; set; }

        public HomePage(IWebDriver driver) : base (driver)
        {

        }

        public void ClickOnNewsButton()
        {
            NewsButton.Click();
        }
    }
}
