﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace Final_Project_BBC1.pages
{

    public class ShareWithBbcNewsPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'checkbox')]//p[contains(text(),'I accept')]")]
        private IWebElement IacceptButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'checkbox')]//p[contains(text(),'I am over 16 years')]")]
        private IWebElement IamOver16YearsButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'button-container')]//button")]
        private IWebElement SubmitButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'input-error-message')]")]
        private IWebElement ErrorMessages { get; set; }


        public ShareWithBbcNewsPage(IWebDriver driver) : base(driver)
        {

        }

        public void ClickOnIacceptButton()
        {
            IacceptButton.Click();
        }

        public void ClickOnIamOver16YearsOldButton()
        {
            IamOver16YearsButton.Click();
        }

        public void ClickOnSubmitButton()
        {
            SubmitButton.Click();
        }

        public IWebElement GetErrorMessages()
        {
            return ErrorMessages;
        }

        public string GetErrorMessageWeb()
        {
            return ErrorMessages.Text;
        }
    }
}
