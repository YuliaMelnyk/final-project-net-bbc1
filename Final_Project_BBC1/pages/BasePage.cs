﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Final_Project_BBC1.pages
{
    public class BasePage
    {
        IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }
    }
}
