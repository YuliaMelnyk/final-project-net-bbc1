﻿using System.Collections.Generic;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Final_Project_BBC1.pages
{
    public class NewsPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'gel-wrap')]/div[contains(@data-entityid,'top-stories#1')]//a[contains(@class,'promo-heading')]//h3")]
        private IWebElement MainArticleWeb { get; set; }
        //div[contains(@class,'inline-block@m')]//a[contains(@class,'promo-heading')]//h3

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'stories__secondary')]//h3")]
        private IList<IWebElement> SecondaryArticleTitles { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'gel-wrap')]/div[contains(@data-entityid,'top-stories#1')]//a[contains(@class,'link--truncate')]//span")]
        private IWebElement CategoryText { get; set; }
        //div[contains(@class,'inline-block@m')]//a[contains(@class,'link--truncate')]//span

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'nav-section')]//input[contains(@placeholder,'Search')]")]
        private IWebElement SearchField { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@id='orb-search-button']")]
        private IWebElement SearchButton { get; set; }        

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'in-container')]//button")]
        private IWebElement SkipPopUpButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'block@m')]//ul[contains(@class,'wide-sections')]//a[contains(@href,'coronavirus')]")]
        private IWebElement CoronavirusButton { get; set; }

        public NewsPage(IWebDriver driver) : base(driver)
        {

        }

        public string GetMainArticleWebTitle()
        {
            return MainArticleWeb.Text;
        }
                
        public List<string> GetSecondaryArticleTitles()
        {
            List<string> articleTitlesWeb = new List<string>();
            foreach (IWebElement title in SecondaryArticleTitles)
            {
                string article = title.Text;
                articleTitlesWeb.Add(article);
            }
            return articleTitlesWeb;
        }

        public string GetCategoryText()
        {
            return CategoryText.Text.ToLower();
            
        }

        public void EnterTheCategory(string category)
        {
            SearchField.SendKeys(category);
        }

        public void SearchByTheCategory()
        {
            SearchButton.Click();
        }        

        public void SkipPopUp()
        {
            SkipPopUpButton.Click();
        }

        public void ClickOnCoronavirusButton()
        {
            CoronavirusButton.Click();
        }
    }
}
