﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Final_Project_BBC1.pages
{
    public class SearchPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "(//div[contains(@class,'l100ew')])[1]//span")]
        private IWebElement SearchArticle { get; set; }

        public SearchPage(IWebDriver driver) : base(driver)
        {

        }

        public string GetSearchArticleTitle()
        {
            return SearchArticle.Text.ToLower();
        }
    }
}
