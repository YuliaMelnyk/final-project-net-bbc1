﻿using Final_Project_BBC1.Business_Logic_Layer;
using Final_Project_BBC1.tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Final_Project_BBC1.hooks
{
    [Binding]
    class Hooks
    {
        private readonly ScenarioContext _scenarioContext;
        private IWebDriver driver;
        public Hooks(ScenarioContext context)
        {
            _scenarioContext = context;
        }

        [BeforeScenario]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.bbc.com/");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SearchNews searchNews = new SearchNews(driver);
            SendNewsToBbc sendNewsToBbc = new SendNewsToBbc(driver);
            _scenarioContext.Add("searchNewsLayer", searchNews);
            _scenarioContext.Add("sendNewsToBbcLayer", sendNewsToBbc);
        }

        [AfterScenario]
        public void CleanUp()
        {
            if (driver != null)
                driver.Quit();
        }
    }
}
